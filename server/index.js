const serveStatic = require('serve-static');
const http = require('http');
const finalhandler = require('finalhandler');

const serverPort = 3000;

const responseDelayRange = { min: 0, max: 2000 };

// Load countries.
console.log('Loading country database...');

const countries = require('./countries.json');

console.log(`Loaded ${countries.length} countries`);

// Create HTTP server.
const serve = serveStatic(`${__dirname}/../client`);

const server = http.createServer((request, response) => {

    if (request.method !== 'GET') {
        response.writeHead(404);
        response.end();
    }

    if (request.url.startsWith('/countries/')) {

        const query = (request.url.substr('/countries/'.length) || '').toLowerCase();

        const results = countries.filter((country) => country.name.toLowerCase().includes(query));

        const responseDelay = Math.round(Math.random() * (responseDelayRange.max - responseDelayRange.min) + responseDelayRange.min);

        setTimeout(() => {
            response.writeHead(200, { 'Content-Type': 'application/json' });
            response.end(JSON.stringify(results));
        }, responseDelay);

    } else {

        serve(request, response, finalhandler(request, response));

    }

});

// Launch HTTP server.

console.log('Starting HTTP server...');

server.listen(serverPort, () => {
    console.log(`HTTP server listening on port ${serverPort}`);
});
