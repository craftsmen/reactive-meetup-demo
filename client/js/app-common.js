function displayCountries(countries) {
	const resultContainerElem = document.querySelector('#resultContainer');

	resultContainerElem.innerHTML = '';

	const listElement = document.createElement('ul');

	countries
		.map((country) => {
			const countryElement = document.createElement('li');
			countryElement.appendChild(document.createTextNode(country.name));
			return countryElement;
		})
		.forEach(listElement.appendChild.bind(listElement));

	resultContainerElem.appendChild(listElement);
}