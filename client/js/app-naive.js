const queryElement = document.querySelector('#searchQueryInput');

queryElement.onkeyup = (keyUpEvent) => {
	const query = keyUpEvent.target.value;

	doSearch(query);
};

function doSearch(query) {
	
	const requestUrl = `countries/${encodeURIComponent(query)}`;

	const xhr = new XMLHttpRequest();
	xhr.open('GET', requestUrl);
	xhr.send();

	xhr.onload = (xhrEvent) => {

		const response = xhrEvent.currentTarget;

		if (response.status === 200) {
			const countries = JSON.parse(response.responseText);
			
			displayCountries(countries);
		}
	};
	
}