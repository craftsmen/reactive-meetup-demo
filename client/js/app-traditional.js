const queryElement = document.querySelector('#searchQueryInput');

let lastRequestId = 0;
let lastTimeoutId = null;
let lastQuery = null;

queryElement.onkeyup = (keyUpEvent) => {
	const query = keyUpEvent.target.value;

	if (lastTimeoutId) {
		clearTimeout(lastTimeoutId);
	}

	lastTimeoutId = setTimeout(() => {
		
		lastTimeoutId = null;

		doSearch(query);

	}, 250);
};

function doSearch(query) {

	if (query.length < 2 || lastQuery === query) {
		return;
	}

	lastQuery = query;
	
	const requestUrl = `countries/${encodeURIComponent(query)}`;

	const xhr = new XMLHttpRequest();
	xhr.open('GET', requestUrl);
	xhr.send();

	const requestId = ++lastRequestId;

	xhr.onload = (xhrEvent) => {
		if (requestId !== lastRequestId) {
			return;
		}

		const response = xhrEvent.currentTarget;

		if (response.status === 200) {
			const countries = JSON.parse(response.responseText);
			
			displayCountries(countries);
		}
	};
	
}