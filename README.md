# Reactive Programming workshop demo application

This repository contains the demo application that was shown at the [Reactive Programming workshop](https://gitlab.com/craftsmen/reactive-meetup) (on 16-12-2016).

## Running the demo application

Use the following steps to run the application:

* Make sure you've got Node.js version 6 or higher installed.

* Download or clone this repository

* Open a console and navigate to the directory where you've copied this repository.

* Execute `npm install` to download the dependencies for the project.

* Execute `npm start` to start the application.

* Open a browser and open the following URL: [http://localhost:3000/](http://localhost:3000/)

You should now be able to see the demo application.

## About the demo application

This demo application provides a simple search interface for country names.
There are three different implementations of the search client:

* A _naive_ implementation.
This implementation uses a 'traditional' imperative programming style, without worrying about debouncing the input, checking for changed inputs or out of order server responses.
The source can be found in [`client/js/app-naive.js`](client/js/app-naive.js).

* A _traditional_ implementation. Same as the naive implementation, but having taken care of the problems outlined above.
The source can be found in [`client/js/app-traditional.js`](client/js/app-traditional.js).

* A _reactive_ implementation. This version also has taken care of the problems above, but is implemented instead using RxJS.
The source can be found in [`client/js/app-reactive.js`](client/js/app-reactive.js).

To switch between these implementations uncomment the desired implementation in [`client/index.html`](client/index.html).

Note we haven't setup anything fancy for this application.
There is no live reload or browser sync, so you'll have to reload the application manually in your browser whenever you make changes.

## Acknowledgments

For this demo we have used a JSON file (maintained by [lukes](https://github.com/lukes)) that contains the ISO-3166 country list.
This JSON file was copied from the following repository:

[https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes](https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes)